package com.atguigu.springcloud.controller;

import com.atguigu.springcloud.common.CommonResult;
import com.atguigu.springcloud.entity.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * @Author: zola
 * @Date: 2023/9/18 - 09 - 18 - 16:39
 * @Description: com.atguigu.springcloud.controller
 * @Version: 1.0
 */
@Slf4j
@RestController
public class OrderController {

    @Autowired
    private RestTemplate restTemplate;

    @PostMapping("/consumer/create/payment")
    public CommonResult<Payment> create(@RequestBody Payment payment){
//        String url = "http://localhost:8001/create/payment";
        String url = "http://cloud-payment-service/create/payment/";
        //使用 postForEntity 内部发的是post请求
        ResponseEntity<CommonResult> commonResultResponseEntity =
                restTemplate.postForEntity(url, payment, CommonResult.class);

        return commonResultResponseEntity.getBody();

    }

    @GetMapping("/consumer/get/payment/{id}")
    public CommonResult<Payment> get(@PathVariable("id") Long id){
//        String url = "http://localhost:8001/get/payment/";
        String url = "http://cloud-payment-service/get/payment/";
        // getForObject() 内部发get 请求
        return restTemplate.getForObject(url+id,CommonResult.class);
    }

    @GetMapping("/consumer/getForEntity/payment/{id}")
    public CommonResult<Payment> getForEntity(@PathVariable("id") Long id){
        String url = "http://cloud-payment-service/get/payment/";
        ResponseEntity<CommonResult> forEntity = restTemplate.getForEntity(url + id, CommonResult.class);
        if (forEntity.getStatusCode().is2xxSuccessful()) {
            return forEntity.getBody();
        }else {
            return new CommonResult(444, "操作失败");
        }
    }

    @GetMapping("/consumer/payment/zipkin")
    public String paymentZipkin() {
        String result = restTemplate.getForObject("http://cloud-payment-service/payment/zipkin/", String.class);
        return result;
    }
}
