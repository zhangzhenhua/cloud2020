package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Author: zola
 * @Date: 2023/10/25 - 10 - 25 - 14:56
 * @Description: com.atguigu.springcloud
 * @Version: 1.0
 */
@EnableConfigServer
@EnableEurekaClient
@SpringBootApplication
public class ConfigServerMain3344 {
    public static void main(String[] args) {
        SpringApplication.run(ConfigServerMain3344.class,args);
    }
}
