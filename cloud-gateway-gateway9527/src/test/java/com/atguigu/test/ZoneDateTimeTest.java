package com.atguigu.test;

import java.time.ZonedDateTime;

/**
 * @Author: zola
 * @Date: 2023/10/24 - 10 - 24 - 16:07
 * @Description: com.atguigu.test
 * @Version: 1.0
 */
public class ZoneDateTimeTest {
    public static void main(String[] args) {
        // 2023-10-24T16:08:07.251+08:00[Asia/Shanghai]
        System.out.println(ZonedDateTime.now());
    }
}
