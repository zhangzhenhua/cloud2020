package com.atguigu.springcloud.controller;

import com.atguigu.springcloud.common.CommonResult;
import com.atguigu.springcloud.entity.Payment;
import com.atguigu.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @Author: zola
 * @Date: 2023/9/18 - 09 - 18 - 16:25
 * @Description: com.atguigu.springcloud.controller
 * @Version: 1.0
 */
@Slf4j
@RestController
public class PaymentController {
    @Autowired
    private PaymentService paymentService;

    @Value("${server.port}")
    private String port;

    @Autowired
    private DiscoveryClient discoveryClient;

    @PostMapping("/create/payment")
    public CommonResult<Payment> create(@RequestBody Payment payment) {
        log.info(payment.toString());
        int i = paymentService.create(payment);
        if (i > 0) {
            return new CommonResult<>(200, "创建成功, 端口："+port);
        } else{
            return new CommonResult<>(444,"创建失败");
        }

    }
    @GetMapping("/get/payment/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id){
        Payment paymentById = paymentService.getPaymentById(id);
        if (paymentById != null){
            return new CommonResult<>(200,"查询成功, 端口："+port,paymentById);
        }else{
            return new CommonResult<>(444,"未查询到");
        }
    }

    @GetMapping("/say")
    public String say(@RequestHeader(name = "X-Request-Id",required = false) String value) {
        return "HelloWord   "+port +"   "+value;
    }


    @GetMapping("/get/lb")
    public String getPaymentLB(){
        return this.port;
    }

    @GetMapping("/payment/discovery")
    public Object discovery(){
        // 获取所有的服务
        List<String> services = discoveryClient.getServices();
        log.info("services:" + services.toString());

        // 获取指定服务下的实例信息
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        for (ServiceInstance instance : instances) {
            log.info("instanceId: " + instance.getInstanceId()+
                    " serviceId: " + instance.getServiceId()+
                    " host: " + instance.getHost()+
                    " port: " + instance.getPort()+
                    " uri: " + instance.getUri());
        }
        return this.discoveryClient;
    }

    @GetMapping(value = "/payment/feign/timeout")
    public String paymentFeignTimeout() {
        // 业务逻辑处理正确，但是需要耗费3秒钟
        try { TimeUnit.SECONDS.sleep(3); } catch (InterruptedException e) { e.printStackTrace(); }
        return port;
    }

    @GetMapping("/payment/zipkin")
    public String paymentZipkin() {
        return "hi ,i'am paymentzipkin server fall back，welcome to atguigu，O(∩_∩)O哈哈~";
    }
}
