package com.atguigu.springcloud.service;

import com.atguigu.springcloud.entity.Payment;

/**
 * @Author: zola
 * @Date: 2023/9/18 - 09 - 18 - 16:24
 * @Description: com.atguigu.springcloud.service
 * @Version: 1.0
 */

public interface PaymentService {
    int create(Payment payment);

    Payment getPaymentById(Long id);
}
