package com.atguigu.springcloud.service;

/**
 * @Author: zola
 * @Date: 2023/10/27 - 10 - 27 - 13:13
 * @Description: com.atguigu.springcloud.service
 * @Version: 1.0
 */
public interface IMessageProvider {

    public String send();

}
