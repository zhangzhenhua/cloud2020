package com.atguigu.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: zola
 * @Date: 2023/10/25 - 10 - 25 - 15:51
 * @Description: com.atguigu.springcloud.controller
 * @Version: 1.0
 */
@RestController
@RefreshScope
public class ConfigController {

    @Value("${server.port}")
    private String serverPort;

    /**
     * 这里取的值是从github上取回来的
     */
    @Value("${config.info}")
    private String configInfo;


    @GetMapping("/test/config/info")
    public String getConfigInfo(){
        return "serverPort:"+serverPort+"\t\n\n configInfo: "+configInfo;
    }
}
