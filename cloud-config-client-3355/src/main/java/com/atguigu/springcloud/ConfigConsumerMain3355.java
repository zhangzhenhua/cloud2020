package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Author: zola
 * @Date: 2023/10/25 - 10 - 25 - 15:49
 * @Description: com.atguigu.springcloud
 * @Version: 1.0
 */
@EnableEurekaClient
@SpringBootApplication
public class ConfigConsumerMain3355 {
    public static void main(String[] args) {
        SpringApplication.run(ConfigConsumerMain3355.class,args);
    }
}
