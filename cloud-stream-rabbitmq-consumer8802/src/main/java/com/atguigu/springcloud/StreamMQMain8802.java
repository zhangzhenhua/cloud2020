package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Author: zola
 * @Date: 2023/10/27 - 10 - 27 - 13:35
 * @Description: com.atguigu.springcloud
 * @Version: 1.0
 */
@EnableEurekaClient
@SpringBootApplication
public class StreamMQMain8802 {
    public static void main(String[] args) {
        SpringApplication.run(StreamMQMain8802.class,args);
    }
}
